gitlab (11.5.7+dfsg-1) unstable; urgency=medium

  * Team upload
  * New upstream version 11.5.7+dfsg
  * Fix CVE-2019-6240: Arbitrary repo read in Gitlab project import
    (Closes: #919822)

 -- Abhijith PA <abhijith@debian.org>  Sun, 20 Jan 2019 21:37:01 +0530

gitlab (11.5.6+dfsg-1) unstable; urgency=high

  * New upstream version 11.5.6+dfsg (Closes: #918086) (Fixes: CVE-2018-20488,
    CVE-2018-20489, CVE-2018-20490, CVE-2018-20491, CVE-2018-20492,
    CVE-2018-20493, CVE-2018-20494, CVE-2018-20495, CVE-2018-20496,
    CVE-2018-20497, CVE-2018-20498, CVE-2018-20499, CVE-2018-20500,
    CVE-2018-20501, CVE-2018-20507)
  * Bump Standards-Version to 4.3.0

 -- Sruthi Chandran <srud@disroot.org>  Thu, 03 Jan 2019 12:56:20 +0530

gitlab (11.5.5+dfsg-1) unstable; urgency=high

  [ Pirate Praveen ]
  * Restart gitlab before checks in postinst (fixes failures in some checks)
  * Explicitly call /usr/bin/bundle to avoid gem installed bundler
  * Move gitlab-rake to /usr/sbin (depends on /sbin/runuser)
  * Remove optional bullet dependency from Gemfile
  * Add Gemfile.autopkgtest to specify test only dependencies
  * Install test only dependencies from rubygems.org for autopkgtest
  * Drop autopkgtest dependencies installed from rubygems.org
  * Relax dependency on rspec-rails
  * Add golang-any as autopkgtest dependency (to build gitlab-shell)
  * Add gitaly: client_path to gitlab.yml

  [ Sruthi Chandran ]
  * New upstream version 11.5.5+dfsg (Fixes: CVE-2018-20229)
  * Add myself to uploaders

 -- Sruthi Chandran <srud@disroot.org>  Fri, 21 Dec 2018 20:27:36 +0530

gitlab (11.5.4+dfsg-1) unstable; urgency=medium

  * New upstream version 11.5.4+dfsg (Fixes: CVE-2018-20144)
  * Add a note about reusing existing system users for Gitlab instance
    (Closes: #916243)
  * Add an example fqdn in debconf template (Closes: #916306)
  * Update minimum version of ruby-nokogiri to 1.8.4
  * Look for changes in /usr/share/rubygems-integration as well for triggering
    Gemfile.lock refresh
  * Remove world write permissions of .yarn-metadata.json files
    (Closes: #915860)
  * Restart gitaly before gitlab:check in postinst

 -- Pirate Praveen <praveen@debian.org>  Sat, 15 Dec 2018 14:43:41 +0530

gitlab (11.5.3+dfsg-1) experimental; urgency=medium

  * New upstream version 11.5.3+dfsg
  * Only gitlab binary needs to be in contrib (Closes: #915759)
  * Tighten dependency on ruby-octokit
  * Set minimum version of ruby-sass to 3.5
  * Refresh patches
  * Update dependencies

 -- Pirate Praveen <praveen@debian.org>  Thu, 13 Dec 2018 14:57:24 +0530

gitlab (11.4.9+dfsg-2) unstable; urgency=medium

  * Reupload to unstable 

 -- Pirate Praveen <praveen@debian.org>  Sat, 08 Dec 2018 12:49:53 +0530

gitlab (11.4.9+dfsg-1) experimental; urgency=medium

  * New upstream version 11.4.9+dfsg
  * Refresh patches
  * Update dependencies
  * Remove gitlab-markup dependency (replaced by github-markup)

 -- Pirate Praveen <praveen@debian.org>  Thu, 06 Dec 2018 13:21:50 +0530

gitlab (11.3.11+dfsg-1) unstable; urgency=high

  * New upstream security release 11.3.11+dfsg (Fixes: CVE-2018-19493,
    CVE-2018-19494, CVE-2018-19495, CVE-2018-19496, CVE-2018-19569,
    CVE-2018-19570, CVE-2018-19571, CVE-2018-19572, CVE-2018-19573,
    CVE-2018-19574, CVE-2018-19575, CVE-2018-19576, CVE-2018-19577,
    CVE-2018-19580, CVE-2018-19583, CVE-2018-19585)

 -- Pirate Praveen <praveen@debian.org>  Thu, 29 Nov 2018 21:57:18 +0530

gitlab (11.3.10+dfsg-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Thu, 22 Nov 2018 20:44:42 +0530

gitlab (11.3.10+dfsg-1) experimental; urgency=medium

  * New upstream version 11.3.10+dfsg (Closes: #914166) (Fixes: CVE-2018-19359)
  * Relax ruby-js-regex version
  * Tighten dependencies (update minimum versions)

 -- Pirate Praveen <praveen@debian.org>  Wed, 21 Nov 2018 11:49:29 +0530

gitlab (11.2.8+dfsg-2) unstable; urgency=medium

  * Add gitlab-rake as a command (Closes: #814506)
  * Revert to using github-linguist 5

 -- Pirate Praveen <praveen@debian.org>  Tue, 20 Nov 2018 10:46:03 +0530

gitlab (11.2.8+dfsg-1) experimental; urgency=medium

  * New upstream version 11.2.8+dfsg
    (Fixes: CVE-2018-18646, CVE-2018-18645, CVE-2018-18641, CVE-2018-18640)

 -- Pirate Praveen <praveen@debian.org>  Sun, 18 Nov 2018 18:42:27 +0530

gitlab (11.1.8+dfsg-2) unstable; urgency=medium

  * Reupload to unstable
  * Add CVEs fixed in 11.1.6 release (Fixes: CVE-2018-16049, CVE-2018-16050,
    CVE-2018-16051, Missing CSRF in System Hooks, Persistent XSS in Pipeline
    Tooltip)

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Nov 2018 12:56:07 +0530

gitlab (11.1.8+dfsg-1) experimental; urgency=medium

  * New upstream version 11.1.8+dfsg
    (Fixes: CVE-2018-17450, CVE-2018-17454, CVE-2018-15472, CVE-2018-17449,
    CVE-2018-17452, CVE-2018-17451, CVE-2018-17453, CVE-2018-17455,
    CVE-2018-17537, CVE-2018-17536)

 -- Pirate Praveen <praveen@debian.org>  Fri, 16 Nov 2018 16:45:36 +0530

gitlab (10.8.7+dfsg-1) unstable; urgency=medium

  * New upstream version 10.8.7+dfsg
    (Fixes: CVE-2018-14602, CVE-2018-14603, CVE-2018-14604, CVE-2018-14605,
    CVE-2018-14606)
  * Refresh and rename patches (to make numbering consistent)
  * Add dependency on bzip2 (Closes: #910058)
  * Add ruby-device-detector as new dependency and tighten dependency versions
  * Remove chmod/chown -R usage in postinst

 -- Pirate Praveen <praveen@debian.org>  Mon, 15 Oct 2018 15:55:10 +0530

gitlab (10.7.7+dfsg-3) unstable; urgency=medium

  * Give gitlab_user ownership of gitlab_data_dir after creating user
    (Closes: #910929)
  * Relax default_value_for, dropzonejs-rails, net-ssh in Gemfile
  * Remove .eslintrc from binary package
  * Add ruby-ed25519 and remove ruby-rbnacl
  * Remove chown -R commands from postinst

 -- Pirate Praveen <praveen@debian.org>  Sat, 13 Oct 2018 22:29:59 +0530

gitlab (10.7.7+dfsg-2) unstable; urgency=medium

  [ Lucas Kanashiro ]
  * Relax recaptcha version (Closes: #907488)

  [ Pirate Praveen ]
  * Reupload to unstable (required protobuf and grpc is now in unstable)
  * Bump dependency on gitaly (for user management via gitlab-common)
  * Bump Standards-Version to 4.2.1 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Sat, 13 Oct 2018 00:11:14 +0530

gitlab (10.7.7+dfsg-1) experimental; urgency=medium

  * New upstream version 10.7.7+dfsg (Fixes: CVE-2018-14364) (Closes: #904026)
  * Bump Standards-Version to 4.2.0 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Mon, 20 Aug 2018 21:38:35 +0530

gitlab (10.7.6+dfsg-2) experimental; urgency=medium

  * Support html-sanitizer >= 2.7.1 (see upstream issue 48415)

 -- Pirate Praveen <praveen@debian.org>  Sat, 30 Jun 2018 16:51:41 +0530

gitlab (10.7.6+dfsg-1) experimental; urgency=medium

  * New upstream version 10.7.6+dfsg
  * Refresh patches
  * Move common dependencies to gitlab-common
  * Don't remove gitlab_data_dir in purge
  * Support upgrading from 8.13 to 10.x (Upstream issue 48040)

 -- Pirate Praveen <praveen@debian.org>  Wed, 27 Jun 2018 16:14:20 +0530

gitlab (10.7.5+dfsg-3) experimental; urgency=medium

  * Relax ruby version check (2.3.3 includes security fixes)
  * Add gitlab-common binary to handle user creation for gitlab and gitaly
    (Closes: #901310)

 -- Pirate Praveen <praveen@debian.org>  Wed, 13 Jun 2018 10:57:34 +0530

gitlab (10.7.5+dfsg-2) experimental; urgency=medium

  * Bump minimum version of gitlab-workhorse to 4.1.0

 -- Pirate Praveen <praveen@debian.org>  Sun, 03 Jun 2018 20:54:56 +0530

gitlab (10.7.5+dfsg-1) experimental; urgency=medium

  * New upstream version 10.7.5+dfsg (Closes: #900522)

 -- Pirate Praveen <praveen@debian.org>  Sun, 03 Jun 2018 19:54:01 +0530

gitlab (10.7.3+dfsg-1) experimental; urgency=medium

  [ Dmitry Smirnov ]
  * CI: experimental CI.
  * CI: origtargz
  * copyright: format URL to HTTPS.
  * watch file to version 4; get tar.bz2 instead of tar.gz;

  [ Pirate Praveen ]
  * Fix filenamemagle as well to use bz2
  * New upstream version 10.7.3+dfsg
  * Refresh patches
  * Upload to experimental (required versions of ruby-rugged and ruby-grpc
    are only available in experimental. libgit2-dev and libgrpc-dev needs
    library transitions and coordination.)

 -- Pirate Praveen <praveen@debian.org>  Sat, 12 May 2018 20:52:12 +0530

gitlab (10.6.5+dfsg-2) unstable; urgency=medium

  * Set minimum version of nodejs to 6 (punycode@2.1.0 needs nodejs >= 6)
  * Use yarn installed webpack
  * Remove symlinks for vendored files (now using it directly)

 -- Pirate Praveen <praveen@debian.org>  Wed, 02 May 2018 18:11:00 +0530

gitlab (10.6.5+dfsg-1) unstable; urgency=medium

  * Use vendored js files (to ease backporting to jessie)
  * Update watch file for new gitlab.com download url pattern
  * New upstream version 10.6.5+dfsg
  * Bump Standards-Version to 4.1.4 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Tue, 01 May 2018 15:13:49 +0530

gitlab (10.6.3+dfsg-3) unstable; urgency=medium

  * Relax dependency on asciidoctor-plantuml

 -- Pirate Praveen <praveen@debian.org>  Sun, 29 Apr 2018 16:59:15 +0530

gitlab (10.6.3+dfsg-2) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * Build-Depends tightening: ruby-grape-entity (>= 0.7.1~)
  * gitlab.yml.example: fixed path to "gitaly.socket".
  * Recommends += gitaly; (Closes: #894015)
  * Fixed versioning of dependencies (ruby-arel (>= 6.0.4~) is crucial).
  * New patch to fix Markdown rendering (Closes: #895871).

  [ Pirate Praveen ]
  * Fix ruby-unf-ext version constraint
  * Update description, webpack is in main now
  * Install all frontend dependencies via npm (to ease backporting to stretch)

 -- Pirate Praveen <praveen@debian.org>  Fri, 27 Apr 2018 13:26:18 +0530

gitlab (10.6.3+dfsg-1) unstable; urgency=medium

  * New upstream version 10.6.3 (Closes: #894867, #894868, #894869)

 -- Pirate Praveen <praveen@debian.org>  Thu, 05 Apr 2018 14:05:46 +0530

gitlab (10.6.2+dfsg-1) unstable; urgency=medium

  * New upstream version 10.6.2
  * Refresh patches
  * Tighten dependency on ruby-loofah
  * Relax dependency on grape-entity (Closes: #894668)

 -- Pirate Praveen <praveen@debian.org>  Wed, 04 Apr 2018 22:27:43 +0530

gitlab (10.6.0+dfsg-1) unstable; urgency=medium

  * New upstream version 10.6.0

 -- Pirate Praveen <praveen@debian.org>  Tue, 27 Mar 2018 20:01:24 +0530

gitlab (10.5.6+dfsg-1) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * Tighten/version dependency ruby-net-ldap:

  [ Pirate Praveen ]
  * New upstream version 10.5.6 (Closes: #893905)
    Fixes: CVE-2018-8801 CVE-2018-8971
  * Tighten dependency on ruby-omniauth-auth0

 -- Pirate Praveen <praveen@debian.org>  Mon, 26 Mar 2018 14:41:54 +0530

gitlab (10.5.5+dfsg-3) unstable; urgency=medium

  * Relax kubeclient dependency
  * Remove plantuml_lexer.rb initializer from /etc (Closes: #893867)

 -- Pirate Praveen <praveen@debian.org>  Fri, 23 Mar 2018 21:01:56 +0530

gitlab (10.5.5+dfsg-2) unstable; urgency=medium

  * Tighten ruby-devise to 4.4.3
  * Remove devDependencies from package.json
  * Start using system node libs, don't add yarn to package.json
  * Isolate yarn to its own directory

 -- Pirate Praveen <praveen@debian.org>  Thu, 22 Mar 2018 21:32:02 +0530

gitlab (10.5.5+dfsg-1) unstable; urgency=medium

  [ Dmitry Smirnov ]
  * Depends += "ruby-excon (>= 0.60.0~)"
  * Added new patch to fix Markdown rendering (Closes: #890757).
  * Depends: set minimum version for "rake".

  [ Pirate Praveen ]
  * New upstream version 10.5.5 (Closes: #888508)
    - Fixes multiple security vulnerabilities in 10.3.4 (CVE-2017-0914,
      CVE-2017-0916, CVE-2017-0917, CVE-2017-0918, CVE-2017-0923,
      CVE-2017-0925, CVE-2017-0926, CVE-2017-0927, CVE-2017-3710)
  * Remove files no longer present in vendor from Files-Excluded
  * Refresh patches
  * Add new node-* dependencies already in the archive as depends
  * Tighten dependencies
  * Bump debhelper compat to 10 and standards version to 4.1.3

 -- Pirate Praveen <praveen@debian.org>  Sun, 18 Mar 2018 15:17:08 +0530

gitlab (9.5.4+dfsg-7) unstable; urgency=medium

  * Reupload to unstable
  * Remove locale directory during purge
  * Locales should be compiled before precompiling assets

 -- Pirate Praveen <praveen@debian.org>  Thu, 01 Mar 2018 23:45:44 +0530

gitlab (9.5.4+dfsg-6) experimental; urgency=medium

  * Relax dependencies in Gemfile for rdoc ruby-prof
  * Build locale and make locale directory writable by symlinking to /var/lib
    (Closes: #890877)
  * Remove work around for libjs-jquery-atwho bug (1.5.4+dfsg.1-2 works)

 -- Pirate Praveen <praveen@debian.org>  Fri, 23 Feb 2018 00:01:33 +0530

gitlab (9.5.4+dfsg-5) experimental; urgency=medium

  * Use jquery from system for compatibility with at.js in node_modules
  * Use document-register-element 1.3.0 (1.7.2 is not compatible)

 -- Pirate Praveen <praveen@debian.org>  Wed, 14 Feb 2018 20:10:18 +0530

gitlab (9.5.4+dfsg-4) experimental; urgency=medium

  * Add workaround for broken libjs-jquery-atwho (#890391)
    (Download at.js from npmjs.com dist tarball)

 -- Pirate Praveen <praveen@debian.org>  Wed, 14 Feb 2018 17:19:26 +0530

gitlab (9.5.4+dfsg-3) experimental; urgency=medium

  [ Dmitry Smirnov ]
  * Fixed dependencies.

  [ Pirate Praveen ]
  * Update npm itself using npm (for @ support in module names)

 -- Pirate Praveen <praveen@debian.org>  Wed, 14 Feb 2018 14:45:11 +0530

gitlab (9.5.4+dfsg-2) experimental; urgency=medium

  * Relax dependencies in Gemfile
  * Tighten dependencies in control 
  * Bump standards
  * Add missing attributions
  * Fix permissions for .gitlab_shell_secret

 -- Pirate Praveen <praveen@debian.org>  Tue, 26 Dec 2017 17:31:57 +0530

gitlab (9.5.4+dfsg-1) experimental; urgency=medium

  * New upstream release
  * Move to contrib (packaging of node modules for front end is not complete)
  * Use npm install for front end dependencies
  * Refresh patches
  * Tighten/update dependencies
  * Update gitlab.yml.example
  * Fix gitlab-mailroom service
  * Update overrides
  * Replace vendored js with system libs

 -- Pirate Praveen <praveen@debian.org>  Wed, 13 Dec 2017 10:50:15 +0530

gitlab (8.13.11+dfsg1-11) unstable; urgency=medium

  * Tighten dependency on ruby-truncato 

 -- Pirate Praveen <praveen@debian.org>  Mon, 14 Aug 2017 12:21:40 +0530

gitlab (8.13.11+dfsg1-10) unstable; urgency=medium

  * Relax dependency on ruby-net-ssh (Closes: #868246) 

 -- Pirate Praveen <praveen@debian.org>  Sun, 30 Jul 2017 16:14:02 +0530

gitlab (8.13.11+dfsg1-9) unstable; urgency=medium

  * Relax dependency on ruby-asana  and ruby-webmock

 -- Pirate Praveen <praveen@debian.org>  Tue, 18 Jul 2017 14:07:05 +0530

gitlab (8.13.11+dfsg1-8) unstable; urgency=medium

  * Export all variables declared in gitlab-debian.conf from
    /etc/default/gitlab (Closes: #863950) Thanks to yyoshino

 -- Pirate Praveen <praveen@debian.org>  Mon, 05 Jun 2017 22:00:03 +0530

gitlab (8.13.11+dfsg1-7) unstable; urgency=medium

  * Correctly bind dbconfig-common configuration file to gitlab package
    (and not to $gitlab_user) in debian/config
  * Revert change in debian/postinst from previous upload which incorrectly
    binds dbconfig-common configuration file to $gitlab_user package

 -- Pirate Praveen <praveen@debian.org>  Fri, 12 May 2017 10:12:55 +0530

gitlab (8.13.11+dfsg1-6) unstable; urgency=medium

  * Remove hard coded gitlab user in postinst (Closes: #862329)
  * Remove dbconfig-common config files on purge

 -- Pirate Praveen <praveen@debian.org>  Thu, 11 May 2017 22:29:06 +0530

gitlab (8.13.11+dfsg1-5) unstable; urgency=medium

  * Fix letsencrypt email handling in config
  * Minor update in postrm output

 -- Pirate Praveen <praveen@debian.org>  Thu, 27 Apr 2017 11:23:43 +0530

gitlab (8.13.11+dfsg1-4) unstable; urgency=medium

  * Check if gitlab_data_dir is defined before using it
  * Ask email address for letsencrypt updates 

 -- Pirate Praveen <praveen@debian.org>  Wed, 26 Apr 2017 21:12:25 +0530

gitlab (8.13.11+dfsg1-3) unstable; urgency=medium

  * Quote variable in test -n (Thanks to Benjamin Drung)

 -- Pirate Praveen <praveen@debian.org>  Fri, 21 Apr 2017 16:02:25 +0530

gitlab (8.13.11+dfsg1-2) unstable; urgency=medium

  * Integrate dbconfig-common (Closes: #859200)
  * Don't set default gitlab user in postinst
  * Change template name from purge to purge_data
  * Switch to runuser from su (runuser correctly handles PAM sessions)

 -- Pirate Praveen <praveen@debian.org>  Fri, 21 Apr 2017 13:16:43 +0530

gitlab (8.13.11+dfsg1-1) unstable; urgency=medium

  [ Balasankar C ]
  * Repack source to remove fuzzaldrin-plus.js (Closes: #858725)

  [ Pirate Praveen ]
  * debian/postrm: Make checks idempotent (use if in place of &&)
  * debian/postrm: Check variables are defined before using them
  * debian/config: pre-seed variables to debconf db from config files
  * debian/postinst:
     - make sure all required variables are present in the config file
     - handle reconfiguration correctly by reapplying variables from debconf db
       to config files
     - Don't touch systemd override.conf if already exist

 -- Pirate Praveen <praveen@debian.org>  Thu, 20 Apr 2017 11:47:49 +0530

gitlab (8.13.11+dfsg-8) unstable; urgency=medium

  * Don't fail if gitlab-debian.defaults not found (to support upgrading
    from older versions)
  * Be more defensive in rm -rf

 -- Pirate Praveen <praveen@debian.org>  Thu, 23 Mar 2017 17:16:50 +0530

gitlab (8.13.11+dfsg-7) unstable; urgency=medium

  [ Balasankar C ]
  * Add patch cve-2017-0882.patch (Fixes: CVE-2017-0882)

  [ Pirate Praveen ]
  * Move gitlab_log_dir variable to /etc (needed got gitlab-unicorn service)

 -- Pirate Praveen <praveen@debian.org>  Tue, 21 Mar 2017 18:28:43 +0530

gitlab (8.13.11+dfsg-6) unstable; urgency=medium

  * Improve configuration file parsing by using source (Closes: #857967) 

 -- Pirate Praveen <praveen@debian.org>  Fri, 17 Mar 2017 22:29:40 +0530

gitlab (8.13.11+dfsg-5) unstable; urgency=medium

  * Move variables used only in maintainer scripts to /usr/lib from /etc
    (Closes: #856606)
  * Use command -v to check existence of dropdb command in postrm

 -- Pirate Praveen <praveen@debian.org>  Tue, 14 Mar 2017 17:21:21 +0530

gitlab (8.13.11+dfsg-4) unstable; urgency=medium

  [ Balasankar C ]
  * Update description to specify that the package is non-omnibus, unlike the
    official one from GitLab.
  * Remove database on purge only if necessary commands are available
    (Closes: #855579)

  [ Pirate Praveen ]
  * Use /usr/lib/gitlab/templates for config file templates used in postinst
    (See 854658#34)
  * Add more checks in postrm to avoid failures which can be ignored

 -- Balasankar C <balasankarc@autistici.org>  Fri, 24 Feb 2017 17:06:52 +0530

gitlab (8.13.11+dfsg-3) unstable; urgency=medium

  * Allow choosing gitlab user (Closes: #854617)
  * Optionally remove all data on purge (Closes: #821087, #839929)

  [ Johannes Schauer ]
  * Amend the README.Debian with instructions of how to upgrade from
    non-Debian installations (Closes: #823743)

 -- Pirate Praveen <praveen@debian.org>  Thu, 16 Feb 2017 17:35:29 +0530

gitlab (8.13.11+dfsg-2) unstable; urgency=medium

  * Use upstream patch for git 2.11 support (Closes: #853251)
  * Set minimum version of gitlab-shell to 3.6.6-3
    (required for git 2.11 support) 

 -- Pirate Praveen <praveen@debian.org>  Tue, 07 Feb 2017 11:24:36 +0530

gitlab (8.13.11+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Remove WoSign from suggested free SSL providers (they stopped providing
    free SSL certificates from September 2016) 
  * Backport git 2.11 support (Closes: #851714)

 -- Pirate Praveen <praveen@debian.org>  Wed, 18 Jan 2017 13:21:17 +0530

gitlab (8.13.6+dfsg2-2) unstable; urgency=medium

  * Add patch cve-2016-9469.diff (Fixes: CVE-2016-9469) (Closes: #847157)
  * Use ruby-jquery-ui-rails 6 (Closes: #847337)
  * Enable more tests
  * Use -C for specifing sidekiq queues, Thanks to Justin F. Hallett
    (Closes: #847114)
  * Add dpkg trigger to refresh Gemfile.lock if a dependency is changed
    (Closes: #847420)

 -- Pirate Praveen <praveen@debian.org>  Sun, 11 Dec 2016 22:06:59 +0530

gitlab (8.13.6+dfsg2-1) unstable; urgency=medium

  * Run tests with RAILS_ENV=test
  * Relax dependency on ruby-grape-entity
  * Remove tasks related to linting from orig.tar 

 -- Pirate Praveen <praveen@debian.org>  Wed, 30 Nov 2016 16:15:29 +0530

gitlab (8.13.6+dfsg1-3) unstable; urgency=medium

  * Fix autopkgtest with upstream patch (Closes: #845656)

 -- Pirate Praveen <praveen@debian.org>  Mon, 28 Nov 2016 23:52:13 +0530

gitlab (8.13.6+dfsg1-2) unstable; urgency=medium

  * Replace embedded copy of fuzzaldrin-plus with libjs-fuzzaldrin-plus
    (Closes: #814871)

 -- Pirate Praveen <praveen@debian.org>  Fri, 25 Nov 2016 22:05:14 +0530

gitlab (8.13.6+dfsg1-1) unstable; urgency=medium

  * New upstream release (Closes: #845180)
  * Replace embedded copy of fuzzaldrin-plus with libjs-fuzzaldrin-plus

 -- Pirate Praveen <praveen@debian.org>  Thu, 24 Nov 2016 15:29:27 +0530

gitlab (8.13.3+dfsg1-2) unstable; urgency=medium

  * Reupload to unstable (Closes: #843519)

 -- Pirate Praveen <praveen@debian.org>  Fri, 11 Nov 2016 10:56:31 +0530

gitlab (8.13.3+dfsg1-1) experimental; urgency=medium

  * New upstream release (Fixes: CVE-2016-9086)
  * Refresh patches
  * Add dependency on lsb-base

 -- Pirate Praveen <praveen@debian.org>  Fri, 04 Nov 2016 16:23:49 +0530

gitlab (8.12.3+dfsg1-1) unstable; urgency=medium

  * New upstream release (Closes: #838256)
  * Use spec.rake and spec.pattern to select tests
  * Use INCLUDE_TEST_DEPENDS variable in Gemfile to select test dependencies
    (--without does not work with --local in bundle install)
  * Move /usr/share/gitlab/.bundle to /var/lib/gitlab
  * Create db/schema.rb only in postinst (Closes: #838668)

  [ Dmitry Smirnov ]
  * Fix failure to start masked gitlab.service after reinstall

 -- Pirate Praveen <praveen@debian.org>  Sat, 01 Oct 2016 15:23:17 +0530

gitlab (8.11.3+dfsg1-3) unstable; urgency=medium

  * Run some of gitlab provided tests as autopkgtests 

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Sep 2016 21:41:51 +0530

gitlab (8.11.3+dfsg1-2) unstable; urgency=medium

  * Use config/initializers/secret_token.rb to create secrets.yml
    (backup your secrets.yml if you are upgrading) 

 -- Pirate Praveen <praveen@debian.org>  Sat, 17 Sep 2016 14:53:04 +0530

gitlab (8.11.3+dfsg1-1) unstable; urgency=medium

  * New upstream release
  * Remove ruby-devise-async dependency 

 -- Pirate Praveen <praveen@debian.org>  Fri, 16 Sep 2016 12:44:05 +0530

gitlab (8.10.5+dfsg-3) unstable; urgency=high

  * Remove ruby-activerecord-deprecated-finders dependency
    (removed from Gemfile)
  * Setting urgency=high as it helps fix #835749

 -- Pirate Praveen <praveen@debian.org>  Wed, 07 Sep 2016 23:18:08 +0530

gitlab (8.10.5+dfsg-2) unstable; urgency=medium

  * Reupload to unstable 

 -- Pirate Praveen <praveen@debian.org>  Thu, 01 Sep 2016 13:17:03 +0530

gitlab (8.10.5+dfsg-1) experimental; urgency=medium

  * New upstream release 
     rouge 2.0 is now compatible (Closes: #830111)
  * Refresh patches
  * Update dependencies

 -- Pirate Praveen <praveen@debian.org>  Wed, 31 Aug 2016 19:24:55 +0530

gitlab (8.9.0+dfsg-10) unstable; urgency=medium

  * Relax dependency on rails in Gemfile (Closes: #834907) 

 -- Pirate Praveen <praveen@debian.org>  Mon, 22 Aug 2016 11:07:21 +0530

gitlab (8.9.0+dfsg-9) unstable; urgency=medium

  * Create gitlab user as system user (Closes: #834037)

  [ Dmitry Smirnov ]
  * New patch to fix error 500 on runners page (Closes: #819903)
    Thanks, Libor Klepáč.
 
 -- Pirate Praveen <praveen@debian.org>  Mon, 15 Aug 2016 19:51:05 +0530

gitlab (8.9.0+dfsg-8) unstable; urgency=medium

  * Update ruby-unicorn-worker-killer dependency
  * Don't fail when .ssh exist

 -- Pirate Praveen <praveen@debian.org>  Tue, 26 Jul 2016 15:03:10 +0530

gitlab (8.9.0+dfsg-7) unstable; urgency=medium

  * Tighten dependencies
  * Allow unicorn 5.0 

 -- Pirate Praveen <praveen@debian.org>  Thu, 21 Jul 2016 13:51:37 +0530

gitlab (8.9.0+dfsg-6) unstable; urgency=medium

  * Create .ssh/authorized_keys in postinst 

 -- Pirate Praveen <praveen@debian.org>  Wed, 20 Jul 2016 23:13:59 +0530

gitlab (8.9.0+dfsg-5) unstable; urgency=medium

  * Tighten dependencies
  * Don't run gitlab:shell:install in postinst (Closes: #831877)
    (installed config.yml works)
  * Add a note about CAcert and browser trust
  
  [ Dmitry Smirnov ]
  * Do not leave dangling symlinks behind after purge
  * Remove generated assets on purge
  * rules: do not install LICENSE files
  * rules: properly use dh-systemd (Closes: #820991)
  * Rewrite terrible upstream .service files:
     - added meta "gitlab.service" that work alike corresponding init.d script
     - new .service files with support for "reload" and propagation of "reload" from "gitlab.service"
     - non-forking PIDFILE-less implementation
    This change fixes services' start-up and postinst error on first install.
  * templates: Replace StartSSL with CACert

 -- Pirate Praveen <praveen@debian.org>  Wed, 20 Jul 2016 20:28:21 +0530

gitlab (8.9.0+dfsg-4) unstable; urgency=medium

  * Move config files to /usr/share/doc as examples and
    copy them to /var/lib for modification (Closes: #821086)
  * Fix gitlab-shell's config.yml handling (Closes: #827846)

 -- Pirate Praveen <praveen@debian.org>  Thu, 14 Jul 2016 19:53:16 +0530

gitlab (8.9.0+dfsg-3) unstable; urgency=medium

  * Relax grape and rouge in Gemfile 

 -- Pirate Praveen <praveen@debian.org>  Sun, 10 Jul 2016 20:23:41 +0530

gitlab (8.9.0+dfsg-2) unstable; urgency=medium

  * Reupload to unstable
  * Relax omniauth-google-oauth2 in Gemfile

 -- Pirate Praveen <praveen@debian.org>  Sat, 09 Jul 2016 20:14:24 +0530

gitlab (8.9.0+dfsg-1) experimental; urgency=medium

  * New upstream release
  * Refresh patches and update dependencies 

 -- Pirate Praveen <praveen@debian.org>  Thu, 23 Jun 2016 23:54:28 +0530

gitlab (8.9.0+dfsg~rc4-1) experimental; urgency=medium

  * New upstream release candidate
  * Refresh patches
  * Use jquery in place of jquery2 (debian has only jquery and it has same API)
  * Remove /etc/gitlab/initializers/devise_async.rb (removed upstream)
  * Symlink /usr/share/gitlab/.ssh to var/lib/gitlab/.ssh

 -- Pirate Praveen <praveen@debian.org>  Wed, 22 Jun 2016 13:57:27 +0530

gitlab (8.8.2+dfsg-5) unstable; urgency=medium

  * Relax dependencies for all stable libraries (>= 1.0) (Closes: #827374) 

 -- Pirate Praveen <praveen@debian.org>  Thu, 16 Jun 2016 12:31:40 +0530

gitlab (8.8.2+dfsg-4) unstable; urgency=high

  * Allow minor updates for ruby-state-machines-activerecord (Closes: #827013)

 -- Pirate Praveen <praveen@debian.org>  Sun, 12 Jun 2016 12:51:19 +0530

gitlab (8.8.2+dfsg-3) unstable; urgency=medium

  * Update minimum version of rails to 4.2.6
  * Update minimum version of ruby-request-store to 1.3
  * Add postgresql-contrib as dependency for pg_trgm extension
    (Closes: #826302)
  * Add ruby-coffee-script-source (>= 1.10.0~) as dependency

 -- Pirate Praveen <praveen@debian.org>  Sun, 05 Jun 2016 18:20:51 +0530

gitlab (8.8.2+dfsg-2) unstable; urgency=medium

  * Move gitlab-shell's config.yml to /etc
  * Update minimum version of gitlab-shell to 3.0.0

 -- Pirate Praveen <praveen@debian.org>  Sat, 04 Jun 2016 21:47:12 +0530

gitlab (8.8.2+dfsg-1) unstable; urgency=medium

  * New upstream release (Closes: #823290)
  * Refresh patches
  * Bump standards version to 3.9.8 (no changes)
  * Enable the pg_trgm extension for postgresql
  * Check if nginx site configuration directory is present before copying
    (Closes: #821085)
  * Symlink /run/gitlab/cache to /var/lib/gitlab/cache (or /run gets filled up)
  * Remove debconf db on purge

 -- Pirate Praveen <praveen@debian.org>  Thu, 02 Jun 2016 22:27:15 +0530

gitlab (8.5.8+dfsg-5) unstable; urgency=medium

  * Make nginx optional (Closes: #819260)
  * Manage nginx configuration via ucf (Closes: #819262)
  * Manage gitlab-debian.conf and gitlab.yml via ucf
  * Make postinst more verbose

 -- Pirate Praveen <praveen@debian.org>  Fri, 08 Apr 2016 17:29:34 +0530

gitlab (8.5.8+dfsg-4) unstable; urgency=medium

  * Tighten version requirements for dependencies
  * Fix permissions for uploads
  * Run db:migrate when db exist
  * Restrict file permissions for secret files (Closes: #819412)
  * Move db to /var/lib/gitlab (fix migrations)

  [ Libor Klepáč ]
  * Create builds directory in /var/log (Closes: #819907)

 -- Pirate Praveen <praveen@debian.org>  Tue, 05 Apr 2016 22:55:36 +0530

gitlab (8.5.8+dfsg-3) unstable; urgency=medium

  * Reupload to unstable
  * Add mail-transport-agent as dependency
  * Symlink 'shared' to /var/lib/gitlab instead of 'shared/cache'
    - gitlab expects everything inside shared to be in the same file system.
  * Use embedded copy of fuzzaldrin-plus (See #814871 for more details)
  * Bring back db check in postinst (initialize the db only if it is empty)
  * Choose unicode for db encoding
  * Don't run letsencrypt if certificate is already present

 -- Pirate Praveen <praveen@debian.org>  Tue, 05 Apr 2016 00:14:30 +0530

gitlab (8.5.8+dfsg-2) experimental; urgency=medium

  * Change letsencrypt from depends to recommends 

 -- Pirate Praveen <praveen@debian.org>  Sun, 03 Apr 2016 11:09:51 +0530

gitlab (8.5.8+dfsg-1) experimental; urgency=medium

  * New upstream release

 -- Pirate Praveen <praveen@debian.org>  Sat, 02 Apr 2016 21:30:27 +0530

gitlab (8.4.3+dfsg-12) unstable; urgency=medium

  [ Libor Klepáč ]
  * Add fix-wiki.patch: Fix wiki display (Closes: #815465)

  [ Pirate Praveen ]
  * Use redis-server.service instead of redis.service (which is an alias to
    redis-server.service and won't be available if redis-server.service is
    disabled) in systemd files.

 -- Pirate Praveen <praveen@debian.org>  Fri, 18 Mar 2016 19:12:10 +0530

gitlab (8.4.3+dfsg-11) unstable; urgency=medium

  * Relax stable library versions requirement
  * Relax dependency on grape-entity

  [ Johannes Schauer ]
  * Add diagnostic info to README.Debian
  * Add steps of migrating a source install to debian package

 -- Pirate Praveen <praveen@debian.org>  Wed, 16 Mar 2016 20:16:47 +0530

gitlab (8.4.3+dfsg-10) unstable; urgency=medium

  [ Balasankar C ]
  * Team upload.
  * Bump Standards Version (No Changes).
  * Fix typo in debian/README.Debian.
  * 0038-relax-net-ssh.patch : Add dep3 header.

  [ Pirate Praveen ]
  * Replace gitlab:setup with db:schema:load and db:seed_fu (Closes: #815798)
  * Relax rails dependency in Gemfile (Closes: #817150)

 -- Pirate Praveen <praveen@debian.org>  Tue, 15 Mar 2016 19:42:59 +0530

gitlab (8.4.3+dfsg-9) unstable; urgency=medium

  * Reorganize directory layout (Closes: #814476)
    - Move gitlab user's home directory to /var/lib
    - Add config as a symlink to /etc/gitlab
    - Add log as a symlink to /var/log/gitlab
    - Add public as a symlink to /var/lib/gitlab/public
    - Add more files that needs write permission to /var

 -- Pirate Praveen <praveen@debian.org>  Thu, 18 Feb 2016 22:51:32 +0530

gitlab (8.4.3+dfsg-8) unstable; urgency=medium

  * Install tmpfiles.d/gitlab.conf and allow www-data user to read /run/gitlab
    (Closes: #814714)
  * Switch to sendmail method by default. 

 -- Pirate Praveen <praveen@debian.org>  Tue, 16 Feb 2016 13:58:16 +0530

gitlab (8.4.3+dfsg-7) unstable; urgency=medium

  * bc should be in build-dep (Closes: #814695)
  * fix pid file mismatch between /etc/default/gitlab and systemd service files
    (Closes: #814714)
  * use tmpfiles.d for /run/gitlab (Closes: #814713) Thanks to Christian Seiler

 -- Pirate Praveen <praveen@debian.org>  Tue, 16 Feb 2016 00:08:18 +0530

gitlab (8.4.3+dfsg-6) unstable; urgency=medium

  * Fix pid and sockets path for gitlab-workhorse 

 -- Pirate Praveen <praveen@debian.org>  Sun, 14 Feb 2016 12:45:24 +0530

gitlab (8.4.3+dfsg-5) unstable; urgency=medium

  * Switch to su from sudo 

 -- Pirate Praveen <praveen@debian.org>  Sat, 13 Feb 2016 23:53:42 +0530

gitlab (8.4.3+dfsg-4) unstable; urgency=medium

  * Don't overwrite existing database (Closes: #814458)
  * Write logs, pids and sockets to /var (Closes: #814476)
  * Add more files to debian/install (Closes: #814503)
  * Add a check in debian/rules for installing all files

 -- Pirate Praveen <praveen@debian.org>  Sat, 13 Feb 2016 21:55:51 +0530

gitlab (8.4.3+dfsg-3) unstable; urgency=medium

  * Check if /run/systemd/system dirctory exist for systemd

 -- Pirate Praveen <praveen@debian.org>  Fri, 12 Feb 2016 01:01:59 +0530

gitlab (8.4.3+dfsg-2) unstable; urgency=medium

  [ Balasankar C ]
  * Add email configuration via sendmail method

  [ Pirate Praveen ]
  * Download certificates via letsencrypt
  * Check /proc/1/cmdline for systemd (Closes: #814413)

 -- Pirate Praveen <praveen@debian.org>  Fri, 12 Feb 2016 00:17:01 +0530

gitlab (8.4.3+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Refresh patches (rails version is in sync)
  * Configure ssl for nginx if selected
  * Use letsencrypt certificate paths if selected
  * Add git as dependency (Closes: #813807)
  * SSH key upload is working (Closes: #812861)
  * Configure gitlab_url as workwround (Closes: #813770 )

 -- Pirate Praveen <praveen@debian.org>  Tue, 09 Feb 2016 23:39:34 +0530

gitlab (8.4.0+dfsg-2) unstable; urgency=medium

  * Add systemd units (Closes: #812841)
  * Remove Gemfile.lock if found before bundle install
    (Closes: #813550) (Closes: #812907)

 -- Pirate Praveen <praveen@debian.org>  Thu, 04 Feb 2016 16:28:26 +0530

gitlab (8.4.0+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Use su instead of sudo everywhere (Closes: #812951)
  * Add rake and ruby-sentry-raven as dependencies
  * Remove patch 0107-relax-omniauth-facebook.patch

 -- Pirate Praveen <praveen@debian.org>  Tue, 02 Feb 2016 19:28:45 +0530

gitlab (8.4.0+dfsg~rc2-4) unstable; urgency=medium

  * Reupload to unstable
  * Relax ruby-net-ssh dependency

 -- Pirate Praveen <praveen@debian.org>  Thu, 28 Jan 2016 02:01:57 +0530

gitlab (8.4.0+dfsg~rc2-3) experimental; urgency=medium

  * Read gitlab-debian.conf from /etc/default/gitlab
    - Use debian specific configuration in init script
  * Add nginx to dependencies (Closes: #812724)
  * Add ruby-influxdb to dependencies (Closes: #812839)
  * Add missing dependecies from Gemfile and tighen versions
  * Use environmental variables by commenting out defaults in gitlab.yml

 -- Pirate Praveen <praveen@debian.org>  Wed, 27 Jan 2016 13:40:31 +0530

gitlab (8.4.0+dfsg~rc2-2) experimental; urgency=medium

  * Add README.Debian to document debian specific changes
  * Make mysql optional (Closes: #812345)
  * Configure hostname via debconf and integrate nginx

 -- Pirate Praveen <praveen@debian.org>  Sun, 24 Jan 2016 15:19:19 +0530

gitlab (8.4.0+dfsg~rc2-1) experimental; urgency=medium

  * Initial release (Closes: #651606)

 -- Pirate Praveen <praveen@debian.org>  Wed, 20 Jan 2016 00:56:59 +0530
