# set -x # use this for debugging

ucount=$(ls -1a |grep -vx .git | wc -l)
dcount=$(cat debian/gitlab.install |cut -d' ' -f1|grep -v debian |wc -l)
ignored=32
if ! [ $(echo "$ucount" - "$dcount"|bc) -eq $ignored ]; then
  echo "Found new files added by upstream and not added to debian/install"
  echo "Add them to debian/gitlab.install or adjust 'ignored=${ignored}'"
  echo "in debian/upstream-file-count-check.sh as required and update"
  echo "debian/upstream-file-list"
  ls -1a |grep -vx .git > debian/upstream-file-list.new
  diff -u debian/upstream-file-list debian/upstream-file-list.new
  exit 1
fi
